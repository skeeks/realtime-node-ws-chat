var app = require('http').createServer(handler)
	, io = require('socket.io').listen(app)
	, fs = require('fs');
app.listen(process.argv[2]|| 3000);
var userList = {};
var autoIncrVal = 0;
function handler (req, res) {
	var pathname = req.url;
	console.log(pathname);
	if(pathname === '/style.css'){
		fs.readFile(__dirname + '/style.css',
	  function (err, data) {
		if (err) {
		  res.writeHead(500);
		  return res.end('Error loading style.css');
		}
		res.setHeader("Content-Type", "text/css");
		res.writeHead(200);
		res.end(data);
	  });
	}
	 else if(pathname === '/jquery.js' || pathname === '/aes.js' || pathname === '/jquery-2.0.3.min.map'){
		fs.readFile(__dirname + pathname,
	  function (err, data) {
		if (err) {
		  res.writeHead(500);
		  return res.end('Error loading ' + pathname);
		}
	  res.setHeader("Content-Type", "text/javascript");
		res.writeHead(200);
		res.end(data);
	  });
	} else if(pathname === '/bg.jpg'){
		fs.readFile(__dirname + pathname,
			'utf-8',
		  function (err, data) {
			if (err) {
			  res.writeHead(500);
			  return res.end('Error loading ' + pathname);
			}
			res.setHeader("Content-Type", "image/png");
			res.writeHead(200);
			res.end(data);
		  });
	} else if(pathname === '/' || pathname === '/index.html' || pathname === '/index' || pathname === '/chat' ||  pathname === '/chat.html'){
	  fs.readFile(__dirname + '/chat.html',
	  function (err, data) {
		if (err) {
		  res.writeHead(500);
		 
		  return res.end('Error loading index.html');
		}
		res.setHeader("Content-Type", "text/html");
		res.writeHead(200);
		res.end(data);
	  });
	}
}

/* -------------------------------------------------------------- */
/* USER MANAGEMENT */
	function genNewUserId(){
		return ++autoIncrVal;
	}
/* ------/* USER MANAGEMENT END *//*--------------------------------- */
/* CONNECTION MANAGEMENT */
io.sockets.on('connection', function (socket) { /* connection event */
	/* ------------- Add the new user to the UserList ---------------- */
	var newUserId = genNewUserId() + "";  // Generate a new user id
	userList[newUserId] = {};	// Set a new user in the user list
	userList[newUserId].username = '';	// set username to ''
	userList[newUserId].socket = socket; 	// set the socket (connection) of the user
	var data = {user_id: newUserId, accepted: true};	// Wrap into a object
	socket.emit('connection-response', data);		// Emit event with data
/* -------------------------------------------------------------- */
socket.on('username-change', function(data){	// onusername-change listener
	var newName = decodeURIComponent(data.username);	// get the new name of the user from request
	var user_id = data.user_id;	
	var valid = true;
	for(var n in userList){ // Foreach loop for userList
		if(userList[n].username.toLowerCase() === newName.toLowerCase() && userList[n].user_id !== user_id){  // Check if the name already exists
			valid = false;
		}
	}
	var responseData;
	if(valid){ // Only set the name if it not already exists
		
		userList[user_id+""].username = newName;	//
		responseData = {accepted: true};	// Wrap response if it's accepted
	} else {
		responseData = {accepted: false}; // Wrap response data if its not accepted to change the name
	}
	
	socket.emit('username-change-response', responseData); // Emit username-change-response event
});
/* -------------------------------------------------------------- */

  socket.on('newmessage', function (data) {
	io.sockets.emit('newmessage', data);
	});
	socket.on('disconnect', function () {
		
	});
});
/* CONNECTION MANAGEMENT */